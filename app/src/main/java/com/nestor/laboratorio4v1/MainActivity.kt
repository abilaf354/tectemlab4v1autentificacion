package com.nestor.laboratorio4v1

import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.Firebase
import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.database


// MainActivity
class MainActivity : AppCompatActivity() {
    lateinit var dbRef: DatabaseReference
    lateinit var recyclerView: RecyclerView
    lateinit var paisesAdapter: itemAdapter // Usando tu adaptador personalizado

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        dbRef = Firebase.database.reference
        recyclerView = findViewById<RecyclerView>(R.id.recycler1)
        recyclerView.layoutManager = LinearLayoutManager(this)
        // Botón para abrir el diálogo
        val btnAdicionar = findViewById<Button>(R.id.btnAdicionar)
        btnAdicionar.setOnClickListener {
            mostrarDialogoParaAdicionar()
        }
    }
        // Mostrar diálogo para adicionar un nuevo país
        private fun mostrarDialogoParaAdicionar() {
            val builder = AlertDialog.Builder(this)
            val dialogView = layoutInflater.inflate(R.layout.item_adicionar, null)
            builder.setView(dialogView)

            val ingresaNombre = dialogView.findViewById<EditText>(R.id.ingresaNombre)
            val ingresaEstado = dialogView.findViewById<EditText>(R.id.ingresaEstado)

            builder.setPositiveButton("Agregar") { dialog, _ ->
                val nombre = ingresaNombre.text.toString()
                val estado = ingresaEstado.text.toString()

                if (nombre.isEmpty() || estado.isEmpty()) {
                    Toast.makeText(this, "Todos los campos son obligatorios", Toast.LENGTH_SHORT).show()
                } else {
                    agregarPais(nombre, estado)
                }
            }

            builder.setNegativeButton("Cancelar") { dialog, _ ->
                dialog.dismiss()
            }

            builder.create().show()
        }

        // Método para agregar un país a Firebase
        private fun agregarPais(nombre: String, estado: String) {
            val id = dbRef.push().key // Generar un nuevo ID
            if (id != null) {
                val nuevoPais = Pais(id, nombre, estado) // Crear el nuevo país
                dbRef.child("paises").child(id).setValue(nuevoPais).addOnSuccessListener {
                    Toast.makeText(this, "País agregado correctamente", Toast.LENGTH_SHORT).show()

                    // Actualizar el RecyclerView
                    listarPaises { paises ->
                        paisesAdapter.updateData(paises)
                        recyclerView.adapter = paisesAdapter
                    }
                }.addOnFailureListener {
                    Toast.makeText(this, "Error al agregar país", Toast.LENGTH_SHORT).show()
                }
            }
        // Listar países y actualizar el RecyclerView con el adaptador
            listarPaises { paises ->
                if (this::paisesAdapter.isInitialized) {
                    paisesAdapter.updateData(paises) // Actualiza la lista en el adaptador
                } else {
                    paisesAdapter = itemAdapter(this, paises.toMutableList()) // Si es la primera vez, crea el adaptador
                    recyclerView.adapter = paisesAdapter
                }
            }

        }

    // Método para listar países desde Firebase y devolver la lista
    fun listarPaises(onResult: (List<Pais>) -> Unit) {
        dbRef.child("paises").addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val paisesList = mutableListOf<Pais>()
                for (snap in dataSnapshot.children) {
                    val pais = snap.getValue(Pais::class.java)
                    if (pais != null) {
                        paisesList.add(pais) // Agregar cada país a la lista
                    }
                }
                onResult(paisesList) // Devolver la lista de países al callback
            }
            override fun onCancelled(databaseError: DatabaseError) {
                // Manejar errores de cancelación si es necesario
            }
        })
    }
    //para refrezcar la lista
    override fun onResume() {
        super.onResume()

        listarPaises { paises ->
            if (this::paisesAdapter.isInitialized) {
                paisesAdapter.updateData(paises)
            } else {
                paisesAdapter = itemAdapter(this, paises.toMutableList())
                recyclerView.adapter = paisesAdapter
            }
        }
    }

}
