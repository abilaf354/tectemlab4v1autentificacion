package com.nestor.laboratorio4v1
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
class itemAdapter(ctx: Context, private var personasModel: MutableList<Pais>) : RecyclerView.Adapter<itemAdapter.PersonasViewHolder>() {

    inner class PersonasViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var nombre: TextView = itemView.findViewById(R.id.mostraNombre)
        var estado: TextView = itemView.findViewById(R.id.mostraEstado)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PersonasViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_lista, parent, false)
        return PersonasViewHolder(v)
    }

    override fun getItemCount(): Int {
        return personasModel.size
    }

    override fun onBindViewHolder(holder: PersonasViewHolder, position: Int) {
        val pais = personasModel[position]
        holder.nombre.text = pais.nombre
        holder.estado.text = pais.id_estado
    }

    // Método para actualizar datos y notificar cambios
    fun updateData(newData: List<Pais>) {
        personasModel.clear() // Limpiar la lista existente
        personasModel.addAll(newData) // Agregar la nueva lista
        notifyDataSetChanged() // Notificar al RecyclerView que los datos han cambiado
    }
}
